require 'optparse'
require_relative 'src/Config'
require_relative 'src/Utils'

if __FILE__ == $0
	help = if ARGV.length == 0 then true else false end
	cfg = SMR::Config.new 'config.json'
	utils = SMR::Utils.new cfg
	
	p = OptionParser.new do |o|
		o.on('-h', '--help', 'show options') { help = true }
		
		o.on('--config-file FILE', 'use the given config file instead of the default "./config.json"') { |f| cfg = SMR::Config.new f; utils = SMR::Utils.new cfg }
		
		o.on('--add-user UNAME,PASS,PRIVS', Array, 'add a new user to the config file') { |u| utils.addUser u[0], u[1], u[2] }
		o.on('--remove-user USERNAME', 'remove the user from authorized users list') { |username| utils.removeUser username }
		o.on('--set-user-privs UNAME,PRIVS', Array, "set priviledges for user #{SMR::USER_PRIVS}") { |up| utils.setUserPrivs up[0], up[1] }
		o.on('--set-user-pass UNAME,PASS', Array, 'set password for user') { |up| utils.setUserPassword up[0], up[1] }
		o.on('--set-max-revs MAX', Integer, 'maximum number of revisions to keep for everything in the repo') { |max| utils.setMaxRevs max }
		o.on('--find-dups', 'find; list duplicate files in the repo') { utils.findDups }
		o.on('--remove-dups', 'destroy any duplicate files in the repo') { utils.removeDups }
		o.on('--init DIR', 'make the directory into a repo (uses current dir if none given by --path option)') { |d| utils.init d }
	end
	
	begin p.parse!
		if help == true
			puts "help(#{help}), ARGV.length: #{ARGV.length}"
			puts p.help()
			exit 0
		end
	rescue OptionParser::InvalidOption => e
		puts "Invalid option: '#{e}'"
		puts parser.help()
		exit 1
	rescue OptionParser::MissingArgument => e
		puts "Missing argument: '#{e}'"
		puts parser.help()
		exit 1
	end
end
