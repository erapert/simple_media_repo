require_relative('../src/Config')
require_relative('../src/Utils')

if __FILE__ == $0
	cfg = SMR::Config.new 'test.json'
	
	puts "maxRevs: #{cfg.maxRevs}"
	puts "setMaxRevs(12): #{cfg.setMaxRevs(12)}"
	puts "metadataPath: #{cfg.metadataPath}"
	puts "users: #{cfg.users.to_s}"
	puts "userExists?('foo'): #{cfg.userExists?('foo')}"
	puts "addUser('bar', 'foo', :admin): #{cfg.addUser('bar', 'foo', :admin)}"
	puts "setUserPrivs('bar', :user): #{cfg.setUserPrivs('bar', :user)}"
	puts "setUserPassword('bar', 'bladdle'): #{cfg.setUserPassword('bar', 'bladdle')}"
	puts "removeUser('bar'): #{cfg.removeUser('bar')}"
	
	begin
		puts "addUser('foo', 'bar', :admin): #{cfg.addUser('foo', 'bar', :admin)}"
	rescue Exception => e
		puts e
	end
	
	puts cfg
	
	puts 'writing new test.json'
	cfg = SMR::Config.new
	cfg.setMaxRevs 10
	cfg.setMetadataPath 'metadata.json'
	cfg.addUser 'admin', 'password', :admin
	cfg.addUser 'user', 'password', :user
	cfg.save 'deleteme.json'
end