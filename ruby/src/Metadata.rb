require 'json'

module SMR
	class Metadata
		def initialize metaPath=nil
			load metaPath if metaPath != nil
		end
		
		def load metafile
			@metaFilename = metafile
			@meta = JSON.parse(File.read(metaPath), { :symbolize_names => true })
		end
		
		def save metafile=nil, metadata=nil
			p = metafile || @metaFilename
			m = metadata || @meta
			File.open(p, 'w') do |f|
				f.write(JSON.pretty_generate(m, { :indent => "\t" }))
			end
		end
		
		# initialize a repo in path (path must be a dir)
		# a metadata.json file will be generated at the top level
		def initRepo path, notes=nil
			n = notes || 'initial commit'
			files = {}
			# for every_file in path
			Dir.glob '**/*' do |p|
				next unless File.file? p
				files[p] = {
					:name => File.basename(p),
					:path => p,
					:revs => [{ :rev => 0, :notes => n }]
				}
			end
			
			meta = { :files => files }
			#puts 'metadata: ', JSON.pretty_generate(meta, { :indent => "\t" })
			save "#{path}/metadata.json", meta
		end
	end
end
