require 'json'

module SMR
	USER_PRIVS = [ 'admin', 'user' ]
	
	class Config
		public
			def initialize path=nil
				@path = ''
				@cfg = {
					:maxRevs => 5,
					:metadataPath => 'metadata.json',
					:users => []
				}
				load path if path != nil
			end
			
			def load path
				@path = path
				@cfg = JSON.parse(File.read(path), { :symbolize_names => true })
			end
			
			def save path=nil
				p = path || @path
				File.open(p, 'w') do |f|
					f.write(JSON.pretty_generate(@cfg,{ :indent => "\t" }))
				end
			end
			
			def maxRevs
				return @cfg[:maxRevs]
			end
			
			def setMaxRevs max
				@cfg[:maxRevs] = max.to_i
				return true
			end
			
			def setMaxRevs! max
				r = setMaxRevs max
				save()
				return r
			end
			
			def users
				return @cfg[:users]
			end
			
			def userExists? uname
				return users.any?{ |u| u[:username] == uname } != false
			end
			
			def addUser uname, pass, privs
				# return false if user already exists
				if userExists? uname
					raise 'User already exists in config'
				end
				
				if !USER_PRIVS.include? privs
					raise "Invalide privs '#{privs}'. Privleges must be one of #{USER_PRIVS}"
				end
				
				# TODO: encrypt the password
				encryptedPassword = pass
				
				@cfg[:users] << {
					:username => uname,
					:password => encryptedPassword,
					:privs => privs
				}
				return true
			end
			
			def addUser! uname, pass, privs
				r = addUser uname, pass, privs
				save()
				return r
			end
			
			def removeUser uname
				i = findUser uname
				@cfg[:users].delete_at i
				return true
			end
			
			def removeUser! uname
				r = removeUser uname
				save()
				return r
			end
			
			def setUserPrivs uname, privs
				if !USER_PRIVS.include? privs
					raise "Invalid privs '#{privs}'. Privleges must be one of #{USER_PRIVS}"
				end
				i = findUser uname
				@cfg[:users][i][:privs] = privs
				return true
			end
			
			def setUserPrivs! uname, privs
				r = setUserPrivs uname, privs
				save()
				return r
			end
			
			def setUserPassword uname, pass
				# TODO: encrypt password
				i = findUser uname
				@cfg[:users][i][:password] = pass
				return true
			end
			
			def setUserPassword! uname, pass
				r = setUserPassword uname, pass
				save()
				return r
			end
			
			def metadataPath
				return @cfg[:metadataPath]
			end
			
			def setMetadataPath path
				@cfg[:metadataPath] = path
			end
			
			def setMetadataPath! path
				r = setMetadataPath path
				save()
				return r
			end
		
		private
			def findUser uname
				index = @cfg[:users].find_index{ |u| u[:username] == uname}
				raise "User not found: '#{uname}'" if index == nil
				return index
			end
	end
end
