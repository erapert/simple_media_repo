require 'socket'
require 'json'

module SMR
	class Server
		def initialize  port, cfg, meta
			@cfg = cfg
			@port = port
			@meta = meta
			@listenSock = TCPServer.open port
			@connections = { :server => @listenSock, :clients => []}
		end
		
		def start
			loop {
				# each client gets its own thread
				Thread.start @listenSock.accept do |client|
					puts "#{client.getaddrinfo()} connected"
					# check username && password, if bad then terminate
					creds = JSON.parse(client.gets.chomp)
					puts "client creds: #{creds[:username]} : #{creds[:password]}"
					@connections[:clients] << client
					# spawn a new thread for each client
						# if uname&&pass good then pass client off to new thread
							# in new thread: start event machine and handle client commands
				end
			}.join
		end
	end
end
