module SMR
	COMMANDS = [
		:sync,
		:delete,
		:add,
		:checkout,
		:submit,
		:revert
	]
	
	class Client
		public
			def initialize cfg
				@cfg = cfg
			end
			
			def runCommands
			end
			
			def login uname, pass
				if tryLogin uname, pass
					@uname = uname,
					@pass = pass
				end
			end
		
		private
			def tryLogin uname, pass
				return false
			end
	end
end
