require_relative './Metadata'

module SMR
	class Utils
		
		def initialize cfg
			@cfg = cfg
		end
		
		def init dir=nil
			d = dir || Dir.pwd
			m = Metadata.new 
			m.initRepo d, 'initial commit'
		end
		
		def addUser uname, pass, privs
			@cfg.addUser! uname, pass, privs
		end
		
		def removeUser uname
			@cfg.removeUser! uname
		end
		
		def setUserPrivs uname, privs
			@cfg.setUserPrivs! uname, privs
		end
		
		def setUserPassword uname, pass
			@cfg.setUserPassword! uname, pass
		end
		
		def setMaxRevs max
			@cfg.setMaxRevs! max
		end
		
		def findDups
			puts "Utils.findDups"
		end
		
		def removeDups
			puts "Utils.removeDups"
		end
	end
end
