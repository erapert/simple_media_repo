#
#	This file is just the launcher
#

require 'optparse'
require_relative 'src/Config'
require_relative 'src/Server'
require_relative 'src/Metadata'
require_relative 'src/Client'
require_relative 'src/Utils'
require_relative 'src/User'

#
#
#
def runServer opts, cfg
	puts "Listening on #{opts[:port]}: #{opts}, #{cfg}"
	
	meta = SMR::Metadata.new cfg[:metadata]
	s = SMR::Server.new opts[:port], cfg, meta
	
	s.start
end

#
#	
#
def runClient opts, cfg
	puts "run client: #{opts}, #{cfg}"
	
	c = SMR::Client.new cfg
	if !c.login opts[:username], opts[:password]
		puts 'Incorrect username or password.'
		exit(2)
	end
	
	# TODO: figure out what commands to run and then do 'em
end

#
#
#
if __FILE__ == $0
	opts = {
		:help => if ARGV.length == 0 then true else false end,
		:utils => [],
		:username => false,
		:password => false,
		:path => false,
		:cfg => 'config.json'
	}
	parser = OptionParser.new do |o|
		o.on('-h', '--help', 'show options') { opts[:help] = true }
		
		# server, config, credentials, connection
		o.on('--serve PORT', 'run the server and listen on the specified port') { |port|
			opts[:server] = true
			opts[:port] = port
		}
		o.on('--config-file [FILE]', 'use the given config file') { |f| opts[:cfg] = f }
		o.on('--user USERNAME', 'run under this username') { |u| opts[:username] = u }
		o.on('--pass PASSWORD', 'use this password') { |p| opts[:password] = p }
		o.on('--host HOST', 'host to connect to (use "local" to use the current directory)') { |h| opts[:host] = h }
		
		# file operations-- applied to PATH
		o.on('--path PATH', 'operate on the given path (a file or dir)') { |p| opts[:path] = p }
		o.on('--sync REV', 'sync to the given revision; use "head" for latest') { |rev| opts[:sync] = rev }
		o.on('--delete [NOTES]', 'remove all revs of file|dir from repo') { |notes| opts[:delete] = notes }
		o.on('--add [NOTES]', 'add file|dir to repo') { |notes| opts[:add] = notes }
		o.on('--checkout', 'check out the file|dir and gain exclusive lock on it') { opts[:checkout] = true }
		o.on('--submit [NOTES]', 'submit the file|dir') { opts[:submit] = notes }
		o.on('--revert REV', 'revert to the previous version or to the given rev') { |rev| opts[:revert] = rev }
	end
	
	begin parser.parse!
		if opts[:help]
			puts parser.help()
			exit 0
		end
		
		cfg = SMR::Config.new opts[:cfg]
		
		if opts[:server]
			runServer opts, cfg
		else
			# try to get uname and pass from local config if it wasn't given on the cmd line
			#if !opts[:username] or !opts[:password]
			#	if cfg.username and cfg.password
			#		opts[:username] = cfg.username
			#		opts[:password] = cfg.password
			#	end
			#end
			#
			if !opts[:username] and !opts[:password]
				puts 'You must log in with a username and password.'
				puts parser.help()
				exit 2
			end
			
			if opts[:path] == false
				puts 'You must specify a path to operate on.'
				puts parser.help()
				exit 1
			end
			
			runClient opts, cfg
		end
		
	rescue OptionParser::InvalidOption => e
		puts "Invalid option: '#{e}'"
		puts parser.help()
		exit 1
	rescue OptionParser::MissingArgument => e
		puts e
		puts parser.help()
		exit 1
	end
end
