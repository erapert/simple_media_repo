# simple_media_repo
super simple version control for media files

<pre>
client:
	required:
		--path [filename || dirname]
	commands:
		--sync ["head" || rev-number]
			(put this rev on my hard drive ('head' is latest rev))
		--delete [notes]
			(remove all revs of file|dir from repo)
		--add [notes]
			(add file|dir to repo and start tracking changes)
		--checkout
			(lock file for all other users and allow only me to change it)
		--submit [notes]
			(upload files|dirs to server with these notes)
		--revert [rev-number]
			(revert files|dirs to previous version or to rev-number if given)

server:
	command-line:
		--serve [port]
			(start the server on specified port number)
		--config-file [config]
			(use this config file)
	
	net-api:
		connection:
			connect (string uname, string pass)
			disconnect ()
		commands:
			sync (string path, uint revision)
			delete (string path, string notes)
			add (file f, string notes)
			checkout (string path)
			submit (file f, string notes)
			revert (string path, uint rev)

utils:
	--find-dups
		(find/list all duplicate files)
	--remove-dups
		(remove duplicate files from repo)
	--init
		(init repo in current dir-- if any files|dirs exist then they're added)
	--add-user [uname] [pass] [privs]
		(add this user to the repo (privs are 'user' || 'admin'))
	--remove-user [uname]
		(remove this user from the repo (all files checked out will be unlocked))
	--set-user-privs [uname] [privs]
		(set the privs level for the specified user)
	--set-user-password [uname] [new_password]
		(set the password for the given user)
	--set-max-revs [max-revs]
		(keep this many revs at most-- delete oldest when adding new)
</pre>
